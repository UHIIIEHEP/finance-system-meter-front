const settings = require('./../settings')

const baseUrl = settings.baseURL;

module.exports = {
  STOCK_LIST: `${baseUrl}/stock/list`,
  
  IDEA_LIST: `${baseUrl}/idea/list`,
  IDEA_DELETE: `${baseUrl}/idea/delete`,
  IDEA_CREATE: `${baseUrl}/idea/create`,
  IDEA_UPDATE: `${baseUrl}/idea/update`,
  IDEA_STATUS_SET: `${baseUrl}/idea/status/set`,

  USER_AUTH: `${baseUrl}/user/auth`,
  USER_SELF_INFO: `${baseUrl}/user/self/info`,

  USER_PERMISSION_CHECK: `${baseUrl}/role/permission/check`,
}
