module.exports = {
  USER_IDEA_LIST: 'user.idea.read',
  USER_IDEA_CREATE: 'user.idea.write',
}
