import {
  USER_IDEA_CREATE,
  USER_IDEA_LIST,
} from './permission';

import IdeaCreatePage from "./../container/pages/IdeaCreatePage";
import IdeaPage from "./../container/pages/IdeaPage";
import PortfolioPage from "./../container/pages/PortfolioPage";

const menu = {
  idea: {
    position: 1,
    name: 'Идеи',
    to: '/',
    permission: USER_IDEA_LIST,
    element: <IdeaPage />
  },
  create_idea: {
    position: 2,
    name: 'Создать идею',
    to: '/ideas',
    permission: USER_IDEA_CREATE,
    element: <IdeaCreatePage />
  },
  portfolio: {
    position: 3,
    name: 'Портфель',
    unvisible: true,
    to: '/portfolio',
    element: <PortfolioPage />
  },
}

export default menu;
