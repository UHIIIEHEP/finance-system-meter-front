import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { userLogin, userLogout } from '../reducer/user.reducer'

import './../styles/user-info.scss'

const UserInfo = () => {
  const dispatch = useDispatch();
  const logout = () => {
    dispatch(userLogin(localStorage.getItem('token')));
    localStorage.setItem('token', '');
    dispatch(userLogout());
  };

  const lastname = useSelector((state) => state.userReducer.lastname);
  const firstname = useSelector((state) => state.userReducer.firstname);
  const patronymic = useSelector((state) => state.userReducer.patronymic);

  const signOut = () => {
    logout()
  }

  return (
    <div className='user-info'>
      <div className='user-info__photo'></div>
      <div className='user-info__user-name'>{`${lastname} ${firstname} ${patronymic}`}</div>

      <button
        className='button'
        onClick={signOut}
      >
        Выйти
      </button>
    </div>
  )
}

export default UserInfo;
