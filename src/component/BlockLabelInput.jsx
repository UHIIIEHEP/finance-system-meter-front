import React from 'react';

import './../styles/block-label-input.scss';

const BlockLabelInput = ({name, type, value, onChange}) => {

  return (
    <div className='block-label-input'>
      <div className='label'>{name}</div>
      <input className='input' type={type} onChange={onChange} value={value}></input>
    </div>
  )
}

export default BlockLabelInput;
