import React from 'react';

import './../styles/block-label-label.scss';

const BlockLabelLabel = ({name, value}) => {

  return (
    <div className='block-label-label'>
      <div className='label'>{name}</div>
      <div className='value'>{value}</div>
    </div>
  )
}

export default BlockLabelLabel;
