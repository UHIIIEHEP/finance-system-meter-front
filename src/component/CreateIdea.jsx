import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';

import BlockLabelInput from './BlockLabelInput'
import BlockLabelLabel from './BlockLabelLabel'
import BlockLabelSelector from './BlockLabelSelector'

import httpHook from './../hooks/http.hook'


import './../styles/create-idea.scss'
import './../styles/app.css'
import formatMoney from '../helper/formatMoney';
import assocData from '../helper/assocData';

import { IDEA_CREATE, STOCK_LIST, } from './../constants/url'

const CreateIdea = () => {

  const token = localStorage.getItem('token');

  const commission = 0.0003;
  const selectedDefaultValue = 'Выбор ЦБ ...';

  const [stock, setStock] = useState([]);
  const [stockId, setStockId] = useState(0);
  const [stockName, setStockName] = useState(selectedDefaultValue);
  const [stockList, setStockList] = useState([]);
  const [buyPrice, setBuyPrice] = useState(0);
  const [salePrice, setSalePrice] = useState(0);
  const [amount, setAmount] = useState(0);

  const [buyCommission, setBuyCommission] = useState(0);
  const [saleCommission, setSaleCommission] = useState(0);
  const [disableBtnAdd, setDisableBtnAdd] = useState(true);

  const onChangeStock = (event) => {
    setStock(event.target.value)
    for (let i = 0; i < stockList.length; i++) {
      if (stockList[i].stock_name === event.target.value) {
        setStockId(stockList[i].stock_id);
        setStockName(stockList[i].stock_name);
        break;
      }
    }
  };

  const onChangebuy = (event) => {
    const price = Number(event.target.value)
    setBuyPrice(price)
    setBuyCommission(price * amount * commission)
  };

  const onChangeSale = (event) => {
    const price = Number(event.target.value)
    setSalePrice(price)
    setSaleCommission(price * amount * commission)
  };

  const onChangeAmount = (event) => {
    const value = Number(event.target.value)
    setAmount(value)
    setBuyCommission(buyPrice * value * commission)
    setSaleCommission(salePrice * value * commission)
  };

  const sendData = async () => {
    const data = {
      stock_id: stockId,
      price_buy: buyPrice,
      price_sale: salePrice,
      amount,
      commission_percent: commission,
    }

    await httpHook(IDEA_CREATE, 'POST', data, { 'Authorization': `Bearer ${token}` });

    clearData();
  }

  const clearData = () => {
    setStockName(selectedDefaultValue)
    setStockId(0);
    setBuyPrice(0);
    setSalePrice(0);
    setAmount(0);
    setBuyCommission(0);
    setSaleCommission(0);
  }

  const getStocks = async () => {

    const response = await httpHook(STOCK_LIST, 'POST', {}, { 'Authorization': `Bearer ${token}` });
    const data = assocData(response.stock)
    const startObj = {
      stock_id: stockId,
      stock_name: stockName,
    }
    setStockList([startObj, ...data]);
  }

  useEffect(() => {
    getStocks();

    if (
      stockId !== 0 &&
      buyPrice !== 0 &&
      salePrice !== 0 &&
      amount !== 0
    ) {
      setDisableBtnAdd(false)
    } else {
      setDisableBtnAdd(true)
    }
  }, [stock, buyPrice, salePrice, amount])


  return (
    <div className='create-idea'>
      <div className='wrap'>
        <div className='block block-input'>
          <BlockLabelSelector
            name='Название'
            data={stockList}
            onChange={onChangeStock}
            value={stockName}
          />
          <BlockLabelInput
            name='Количество'
            type='number'
            onChange={onChangeAmount}
            value={amount}
          />
          <BlockLabelInput
            name='Цена покупки'
            type='number'
            onChange={onChangebuy}
            value={buyPrice}
          />
          <BlockLabelInput
            name='Цена продажи'
            type='number'
            onChange={onChangeSale}
            value={salePrice}
          />
        </div>
      </div>
      <div className='wrap wrap-right'>
        <div className='block block-output'>
          <BlockLabelLabel
            name='Комиссия покупки'
            value={formatMoney(buyCommission)}
          />
          <BlockLabelLabel
            name='Комиссия продажи'
            value={formatMoney(saleCommission)}
          />
        </div>
        <div className='block-total'>
          ИТОГО:  {formatMoney(Math.round(((salePrice - buyPrice) * amount - buyCommission - saleCommission) * 1000) / 1000)}
        </div>

        <div className='block-button'>
          <button className={`button button-add ${disableBtnAdd ? 'button-disable' : ''}`} onClick={sendData}>Сохранить</button>
          <button className='button button-clear' onClick={clearData}>Очистить</button>
        </div>
      </div>
    </div>
  )
}

export default CreateIdea;
