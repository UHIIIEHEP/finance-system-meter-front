import React from 'react';

import './../styles/block-label-selector.scss';

const BlockLabelSelector = ({ name, data, value, onChange }) => {

  const options = () => {
    return data.map((item, index) => (<option key={index}>{item.stock_name}</option>))
  }

  return (
    <div className='block-label-selector'>
      <div className='label'>{name}</div>
      <select className='selector' onChange={onChange} value={value}>
        {options()}
      </select>
    </div>
  )
}

export default BlockLabelSelector;
