import React from 'react';

import MenuItem from './MenuItem';

import menu from '../constants/menu';

import './../styles/menu.scss'
import checkPermission from '../helper/checkPermission';

const Menu = () => {

  const createMenu = () => {

    const content = Object.keys(menu).map((item, index) => {
      if (!menu[item].unvisible && checkPermission(menu[item].permission)) {
        return (
          <MenuItem
            key={index}
            to={menu[item].to || '/'}
            value={menu[item].name}
          />
        )
      }
    })

    return (
      <div className='menu'>{content}</div>
    )
  }

  return (
    <div>
      {createMenu()}
    </div>
  )
}

export default Menu;
