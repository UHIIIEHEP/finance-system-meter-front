import React from 'react';
import { Link } from 'react-router-dom';

import './../styles/menu-item.scss'

const MenuItem = ({value, to}) => {

  const btnClick = () => {
    console.log({value})
  }

  return (
    <Link to = {to}>
      <button
        name = {value}
        className = 'menu-item'
        onClick = {btnClick}
      >
        {value}
      </button>
    </Link>
  )
}

export default MenuItem;
