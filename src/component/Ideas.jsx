import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import httpHook from './../hooks/http.hook'
import BlockLabelInput from './BlockLabelInput'
import BlockLabelLabel from './BlockLabelLabel'

import Table from './Table';
import { IDEA_LIST, IDEA_DELETE, IDEA_UPDATE, IDEA_STATUS_SET } from './../constants/url'

import './../styles/ideas.scss'

const Ideas = () => {

  const [ideas, setIdeas] = useState([[], [[]]]);
  const [stockName, setStockName] = useState(null);
  const [priceSale, setPriceSale] = useState(0);
  const [realPriceSale, setRealPriceSale] = useState(0);
  const [priceBuy, setPriceBuy] = useState(0);
  const [realPriceBuy, setRealPriceBuy] = useState(0);
  const [ideaId, setIdeaId] = useState(0);
  const [visibleModalSale, setVisibleModalSale] = useState(false);
  const [visibleModalBuy, setVisibleModalBuy] = useState(false);

  // const token = useSelector((state) => state.userReducer.token);
  const token = localStorage.getItem('token');

  const getIdeas = async () => {

    console.log({ token })

    const response = await httpHook(IDEA_LIST, 'POST', {}, { 'Authorization': `Bearer ${token}` });
    const idea = response.idea;

    const position = idea[0].indexOf('idea_id');
    const positionStatus = idea[0].indexOf('status');
    const positionName = idea[0].indexOf('stock_name');
    const positionPriceBuy = idea[0].indexOf('price_buy');
    const positionPriceSale = idea[0].indexOf('price_sale');

    idea[0].push('deleted');
    idea[1].forEach(item => {
      if (item[positionStatus] === 'archve') {
        item.push(<div className='button-wrapper'></div>)
      } else {
        item.push(<div className='button-wrapper'><button className='button delete' onClick={deleteIdea} idea-id={item[position]}>Удалить</button></div>)
      }
    });

    idea[0].push('status_button');
    idea[1].forEach(item => {
      if (item[positionStatus] === 'ready') {
        item.push(<div className='button-wrapper'><button className='button status_button' onClick={inWorkIdea} idea-id={item[position]} name={item[positionName]} price_buy={item[positionPriceBuy]}>В работу</button></div>)
      } else if (item[positionStatus] === 'in_work') {
        item.push(<div className='button-wrapper'><button className='button status_button' onClick={releaseIdea} idea-id={item[position]} name={item[positionName]} price_sale={item[positionPriceSale]}>Реализованно</button></div>)
      } else if (item[positionStatus] === 'archve') {
        item.push(<div className='button-wrapper'>В АРХИВЕ</div>)
      } else {
        item.push(<div className='button-wrapper'>ОТРАБОТАНО</div>)
      }
    });

    setIdeas(idea);
  }

  const deleteIdea = async (event) => {
    await httpHook(IDEA_DELETE, 'POST', { idea_id: [event.target.attributes['idea-id'].value] }, { 'Authorization': `Bearer ${token}` });

    console.log('ARCHIVE ', event.target.attributes['idea-id'].value)
    await httpHook(IDEA_STATUS_SET, 'POST', { idea_id: event.target.attributes['idea-id'].value, status: 'archive' }, { 'Authorization': `Bearer ${token}` });
    getIdeas();
  }

  const inWorkIdea = async (event) => {
    console.log('IN WORK ', event.target.attributes['idea-id'].value);
    setStockName(event.target.attributes['name'].value);
    setPriceBuy(event.target.attributes['price_buy'].value);
    setIdeaId(event.target.attributes['idea-id'].value);
    setVisibleModalBuy(true)
    // await httpHook(IDEA_STATUS_SET, 'POST', { idea_id: event.target.attributes['idea-id'].value, status: 'in_work' }, { 'Authorization': `Bearer ${token}` });

    getIdeas();
  }

  const releaseIdea = async (event) => {
    setStockName(event.target.attributes['name'].value);
    setPriceSale(event.target.attributes['price_sale'].value);
    setIdeaId(event.target.attributes['idea-id'].value);
    setVisibleModalSale(true);
  }

  const updateIdeaSale = async () => {
    await httpHook(IDEA_STATUS_SET, 'POST', { idea_id: ideaId, status: 'release' }, { 'Authorization': `Bearer ${token}` });
    await httpHook(IDEA_UPDATE, 'POST', { idea_id: ideaId, price_sale: realPriceSale }, { 'Authorization': `Bearer ${token}` });

    getIdeas();
    setVisibleModalSale(false);
  }

  const updateIdeaBuy = async () => {
    await httpHook(IDEA_STATUS_SET, 'POST', { idea_id: ideaId, status: 'in_work' }, { 'Authorization': `Bearer ${token}` });
    await httpHook(IDEA_UPDATE, 'POST', { idea_id: ideaId, price_buy: realPriceBuy }, { 'Authorization': `Bearer ${token}` });

    getIdeas();
    setVisibleModalBuy(false);
  }

  const cancel = async () => {
    setVisibleModalSale(false);
    setVisibleModalBuy(false);
  }

  const onChangePriceSale = (event) => {
    const value = Number(event.target.value)
    setRealPriceSale(value)
  };

  const onChangePriceBuy = (event) => {
    const value = Number(event.target.value)
    setRealPriceBuy(value)
  };

  const sequence = [
    'row_number',
    'stock_name',
    'date',
    'amount',
    'price_buy',
    'price_sale',
    'commission_buy',
    'commission_sale',
    'status_button',
    'deleted',
  ];

  const headMap = {
    row_number: 'пп',
    amount: 'Количество',
    date: 'Дата',
    stock_name: 'Название',
    price_buy: 'Цена покупки',
    price_sale: 'Цена придажи',
    commission_buy: 'Комиссия покупки',
    commission_sale: 'Комиссия продажи',
    status_button: 'Статус',
    deleted: 'Удалить',
  }

  useEffect(() => {
    getIdeas();
  }, [])


  return (
    <div className='ideas'>
      {
        visibleModalSale &&
        <div className='pop-up'>
          <BlockLabelInput
            name='Фактическая цена:'
            type='number'
            onChange={onChangePriceSale}
            value={realPriceSale || priceSale}
          />
          <button
            className='button'
            onClick={updateIdeaSale}
          >
            Зафиксировать
          </button>
          <button
            className='button'
            onClick={cancel}
          >
            Отмена
          </button>
        </div>
      }
      {
        visibleModalBuy &&
        <div className='pop-up'>
          <BlockLabelInput
            name='Фактическая цена:'
            type='number'
            onChange={onChangePriceBuy}
            value={realPriceBuy || priceBuy}
          />
          <button
            className='button'
            onClick={updateIdeaBuy}
          >
            Зафиксировать
          </button>
          <button
            className='button'
            onClick={cancel}
          >
            Отмена
          </button>
        </div>
      }
      <Table
        head={ideas[0]}
        body={ideas[1]}
        headMap={headMap}
        sequence={sequence}
      />
    </div>
  )
}

export default Ideas;
