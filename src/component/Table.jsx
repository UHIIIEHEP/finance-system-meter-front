
import './../styles/table.scss'

const Table = ({ head, body, headMap, sequence }) => {
  const length = head.length;

  const thead = [];
  thead.length = length;

  const tbody = body.map(() => {
    return [];
  });

  sequence.forEach((item, index) => {
    const position = head.indexOf(item);
    if (position !== -1) {
      thead.push(<div className={`td td__${head[position]}`} key={index}>{headMap[head[position]]}</div>)

      for (let i = 0; i < body.length; i++) {
        tbody[i].push((<div className={`td td__${head[position]}`} key={index}>{body[i][position]}</div>))
      }
    }
  })

  return (
    <div className='table'>
      <div className='thead'>
        {thead}
      </div>
      <div className='tbody'>
        {tbody.map((item, index) => {
          return (
            <div key={index} className={`tr tr__index-${index % 2}`}>
              {item}
            </div>
          )
        })}
      </div>
    </div>
  )
}

export default Table;
