import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { useCookies } from 'react-cookie';

import httpHook from './../hooks/http.hook'

import BlockLabelInput from './BlockLabelInput';

import { USER_AUTH, USER_SELF_INFO } from './../constants/url';

import { userLogin, userSelfInfo } from '../reducer/user.reducer'


const Auth = () => {

  const dispatch = useDispatch();

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [token, setToken] = useState('');

  const [cookies, setCookie] = useCookies(['user']);

  const login = () => {
    setCookie('token', token, { path: '/' });
    if (token !== '') {
      // localStorage.setItem('token', token);
      dispatch(userLogin(token));
    }
  };

  const onChangeEmail = (event) => {
    const value = event.target.value;
    setEmail(value)
  };

  const onChangePassword = (event) => {
    const value = event.target.value;
    setPassword(value)
  };

  const signIn = async () => {
    const response = await httpHook(USER_AUTH, 'POST', { email, password });

    setToken(response.token);
    const userInfo = await httpHook(USER_SELF_INFO, 'POST', { email, password }, { 'Authorization': `Bearer ${response.token}` });
    dispatch(userSelfInfo(userInfo.user));
  }

  useEffect(() => {
    login();
  })

  return (
    <div className='auth'>
      <BlockLabelInput
        name='Email: '
        type='text'
        onChange={onChangeEmail}
        value={email}
      />
      <BlockLabelInput
        name='Пароль: '
        type='password'
        onChange={onChangePassword}
        value={password}
      />
      <button
        className='button'
        onClick={signIn}
      >
        Войти
      </button>
    </div>
  )
}

export default Auth;
