import React from 'react';
import Menu from '../component/Menu'

import './../styles/left-block.scss';

const LeftBlock = () => {

  return (
    <div className = 'left-block'>
      <div className = 'left-block__logo'>
        ФинСиМетр
      </div>
      <Menu />
    </div>
  )

}

export default LeftBlock;
