import React from 'react';

import UserInfo from './../component/UserInfo';

import './../styles/header.scss'

const Header = () => {

  return (
    <div className = 'header'>
      <UserInfo />
    </div>
  )

}

export default Header;
