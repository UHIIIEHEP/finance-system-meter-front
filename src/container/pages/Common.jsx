import { BrowserRouter, Routes, Route } from "react-router-dom";
import { useSelector } from 'react-redux';
import { useEffect, useState } from "react";
import { useCookies } from 'react-cookie';

import LeftBlock from './../LeftBlock'
import Header from './../Header'
import AuthPage from "./../pages/AuthPage";

import menu from './../../constants/menu';
import checkPermission from './../../helper/checkPermission';

const CommonPage = () => {

  const [authState, setAuthState] = useState(false);
  const [cookies, setCookie] = useCookies(['user']);

  // const token = useSelector(state => state.userReducer.token);
  // const user = useSelector(state => state.userReducer.user);

  console.log({ cookies: cookies.token }, cookies.token !== '')

  useEffect(() => {
    // if (cookies.token === '') {
    //   setAuthState(true);
    // } else {
    //   setAuthState(false);
    // }
  })

  return (
    <BrowserRouter>
      <div className='app'>
        {true &&
          <AuthPage />
        }

        {true &&
          <>
            <div className='left'>
              <LeftBlock />
            </div>
            <div className='right'>
              <Header />
              <Routes>
                {
                  Object.keys(menu).map((item, index) => {
                    if (!menu[item].unvisible && checkPermission(menu[item].permission, 1/*user.user_id*/)) {
                      return (
                        <Route key={index} path={menu[item].to} element={menu[item].element}></Route>
                      )
                    }
                  })
                }
              </Routes>
            </div>
          </>
        }
      </div>
    </BrowserRouter>
  )
}

export default CommonPage;