
import React from 'react';
import Ideas from '../../component/Ideas';

import './../../styles/ideas.scss';

const IdeaPage = () => {

  return (
    <div className='idea'>
      <Ideas />
    </div>
  )
}

export default IdeaPage;
