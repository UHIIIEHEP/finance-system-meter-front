
import React from 'react';
import CreateIdea from '../../component/CreateIdea';

import './../../styles/ideas.scss';

const IdeaCreatePage = () => {

  return (
    <div className='idea'>
      <CreateIdea />
    </div>
  )
}

export default IdeaCreatePage;
