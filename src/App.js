import { Provider } from 'react-redux';
import store from './reducer';
import { CookiesProvider } from 'react-cookie';

import './styles/app.css';
import CommonPage from "./container/pages/Common";

function App() {
  return (
    <Provider store={store}>
    <CommonPage />
    </Provider>
  )
}

export default App;
