import { combineReducers } from 'redux';
import { createStore} from 'redux';

import userReducer from './user.reducer'


const rootReducer = combineReducers({
  userReducer,
});

const store = createStore(rootReducer);

export default store;
