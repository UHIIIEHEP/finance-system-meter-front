const USER_INFO = 'USER_INFO';
const USER_LOGIN = 'USER_LOGIN';
const USER_LOGOUT = 'USER_LOGOUT';

const defaultState = {
  firstname: 'Иван',
  lastname: 'Иванов',
  patronymic: 'Иванович',
  user_id: -1,

  token: '',
};

const userReducer = (state = defaultState, action) => {
  switch(action.type) {
    case USER_INFO:
      return {
        ...state,
        firstname: action.payload.firstname,
        lastname: action.payload.lastname,
        patronymic: action.payload.patronymic,
        user_id: action.payload.user_id,
      }

    case USER_LOGIN:
      return {
        ...state,
        token: action.payload,
      }

    case USER_LOGOUT:
      return {
        ...state,
        token: action.payload,
      }

    default:
      return state;
  }
};

export default userReducer;

export const userSelfInfo = (user) => ({
  type: USER_INFO,
  payload: user,
})


export const userLogin = (token) => ({
  type: USER_LOGIN,
  payload: token,
})

export const userLogout = () => ({
  type: USER_LOGOUT,
  payload: '',
})
