import axios from 'axios';

const httpHook = async (url, method, payload = {}, headers = {}) => {
  const settings = {
    url,
    method,
    data: payload,
    headers,
  }

  const response = await axios.request(settings)
    .then((res) => res.data.result)
    .catch(function (error) {
      console.log({ error });
    });

    return response;
}

export default httpHook;
