import httpHook from './../hooks/http.hook';
import { USER_PERMISSION_CHECK } from './../constants/url';

const checkPermission = async (alias, relation, relation_id) => {
  const token = localStorage.getItem('token');

  if (token === '') return false;

  const allow = await httpHook(USER_PERMISSION_CHECK, 'POST', {alias, relation, relation_id}, { 'Authorization': `Bearer ${token}` });

  return allow;
}

export default checkPermission;
