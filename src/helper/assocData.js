module.exports = (data) => {
  if (data.length === 0) return {};

  return data[1].map((item) =>{
    const obj = {};
    data[0].forEach((key, index) => {
      obj[key] = item[index];
    })

    return obj;
  })
}
