module.exports = (value) => {

  // return value;
  
  if (value > Math.pow(10, 17)) return 'Unreal number' 

  const str = String(Math.floor(value));

  const digit = Math.floor((str.length-1) / 3);

  // const digit = dataDigit > 4 ? 4 : dataDigit;

  const unitMap = {
    0: '',
    1: ' тыс.',
    2: ' млн.',
    3: ' млрд.',
    4: ' тлрд.',
  }
  const unit = unitMap[digit]
  const num = Math.round((value / Math.pow(10, (3 * digit)))*Math.pow(10,4)) / Math.pow(10,4);

  return num + unit;
}
